import scalatags.Text.svgAttrs._
import scalatags.Text.svgTags.{path, _}
import scalatags.Text.implicits._
import scala.util.Random
import Math._

object Main extends App {
  val margin: Double = 0.17

  val pi: Double = 3.14159265359
  val a: Double = pi/4 - margin

  val innerDia: Double = 5000
  val outerDia: Double = 5000

  def segment(a1: Double, a2: Double, r1: Double, r2: Double) =
    s"M ${sin(a1)*r1},${-cos(a1)*r1} ${sin(a1)*(r1+r2)},${-cos(a1)*(r1+r2)} " +
    s"A ${r1+r2} ${r1+r2} ${a2 - a1} 0 1 ${sin(a2)*(r1+r2)},${-cos(a2)*(r1+r2)} " +
    s"L ${sin(a2)*r1},${-cos(a2)*r1} " +
    s"A $r1 $r1 ${a2 - a1} 0 0 ${sin(a1)*r1} ${-cos(a1)*r1} Z "

  val segmentArc = pi/4-margin
  def innerSegment(a1: Double, a2: Double, s: Int, x: Int, y: Int, r1: Double, r2: Double, recurse: Option[Int]): String = {
    val a1p = a1 + x * (a2 - a1) / s
    val a2p = a1 + (x + 1) * (a2 - a1) / s
    val r1p = r1 + y * r2 / s
    val r2p = r2 / s
    recurse match {
      case None =>
        segment(a1p, a2p, r1p, r2p)
      case Some(s) => 
        fillRandomSegment(a1p, a2p, s, r1p, r2p, None)
    }
  }
  def innerSegment(n: Int, s: Int, x: Int, y: Int, recurse: Option[Int] = None): String =
    innerSegment(n*pi/4-segmentArc/2, n*pi/4+segmentArc/2, s, x, y, innerDia, outerDia, recurse)

  val outlines = (0 to 7).map(i =>
    path(style := "fill:none;stroke:black;stroke-width:100", d := segment(-(pi/4-margin)/2 + i*pi/4, (pi/4-margin)/2 + i*pi/4, innerDia, outerDia))
  )

  val random = new Random(42)

  def fillRandomSegment(a1: Double, a2: Double, s: Int, r1: Double, r2: Double, recurse: Option[Int]) = {
    (0 to s-1).map { x =>
      (0 to s-1).map { y =>
        if (random.nextBoolean()) None
        else Some(
          innerSegment(a1, a2, s, x, y, r1, r2, recurse)
        )
      }
    }.flatten.collect { case Some(p) => p }.mkString
  }
  def fillRandomSegment(n: Int, s: Int, r1: Double, r2: Double, recurse: Option[Int] = None): String =
    fillRandomSegment(n*pi/4-segmentArc/2, n*pi/4+segmentArc/2, s, r1, r2, recurse)

  val inner = (0 to 7).map(_ match {
    case 0 =>
      path(style := "fill:#5e5e5e;stroke:none", d := fillRandomSegment(8, pow(8, 1.9).toInt, innerDia, outerDia))
    case 1 =>
      val recurse = Some(28)
      path(style := "fill:#5d5d5d;stroke:none", d :=
        innerSegment(1, 2, 0, 0, recurse) + innerSegment(1, 2, 1, 1, recurse)
      )
    case 2 =>
      val recurse = Some(28)
      path(style := "fill:#555555;stroke:none", d :=
        innerSegment(2, 3, 0, 0, recurse) + innerSegment(2, 3, 1, 0, recurse) + innerSegment(2, 3, 2, 0, recurse) + innerSegment(2, 3, 2, 1, recurse) + innerSegment(2, 3, 1, 2, recurse)
      )
    case n =>
      val recurse = n match {
        case 3 => Some(20)
        case 4 => Some(15)
        case 5 => Some(10)
        case 6 => Some(10)
        case _ => None
      }
      val color = n match {
        case 7 => "#777777"
        case _ => "black"
      }
      path(style := s"fill:$color;stroke:none", d :=
        fillRandomSegment(n, pow(n.toDouble, 2).toInt, innerDia, outerDia, recurse)
      )
  })

  val result = svg(
    xmlns := "http://www.w3.org/2000/svg",
    viewBox := "-10000,-10000,20000,20000",
    g(
      transform := "rotate(-90)",
      g((inner ++ outlines) :_*)
    )
  )

  import java.io._
  val pw = new PrintWriter(new File("circle.svg" ))
  pw.write("""<?xml version="1.0" encoding="UTF-8" standalone="no"?>""")
  pw.write("\n")
  pw.write(result.render)
  pw.close
}
